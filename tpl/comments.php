<?php

/**
 *
 * отображение страницы комментариев
 *
 */

?>

<br>
<br>
<br>

<!-- all posts in this blog -->
<div class="container-fluid">
    <div class="container-weight">
        <div class="all_comment">
            <? foreach ($this->comments as $key => $value): ?>
                <div class="one_comment">
                    <div class="text"><?=$value['text']?></div>
                    <span class="name"><?=$value['author'] ?></span>
                    <span class="date"><?='&ensp;'.date('Y-m-d H:i:s', $value['create_at'])?></span>
                    <br>
                    <a href="/?del/<?=$value['id']?>" class="btn btn-mini btn-danger" onclick="return confirm('Точно удалить?');">удалить</a>
                </div>
            <? endforeach ?>
        </div>
    </div>
</div><!-- / posts -->