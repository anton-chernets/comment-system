<?php

/**
 *
 * отображение главной страницы
 *
 * @var $title string
 *
 */

$title = 'comment system';

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Php Task">
    <meta name="author" content="Антон Чернец">

    <title><?= $title ?></title>

    <!-- favicon -->
    <link href="/img/favicon.ico" type="image/x-icon" rel="shortcut icon">

    <!-- Le styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>

    <!--personal plagins -->
    <link href="/css/main.css" rel="stylesheet">

  </head>

  <body>
  <!-- preloader -->
  <div class="loader">
    <div class="loader_inner"></div>
  </div>

    <!-- navbar -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-weight">
          <!-- nav -->
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/">comment system</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/?add">Добавить комментарий</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">

      <? $this->out($this->tpl,true); ?>

    </div> <!-- /container -->

  <footer class="navbar-fixed-bottom footer">
    <div class="container-fluid">
      <div class="container-weight">
        <div class="main_footer">
          <span class="copy">&copy; Антон Чернец <?= date('Y') ?></span>
        </div>
      </div>
    </div>
  </footer><!-- /footer -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/main.js"></script>
    <script src="/js/bootstrap.min.js"></script>

  	</body>
</html>