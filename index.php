<?php
/**
 *
 * Методы для подключения файлов
 * @read http://php.net/manual/ru/function.require.php
 */
require(__DIR__ . '/lib/db.php');
require(__DIR__ . '/lib/base.php');

/**
 * Класс application - web приложения
 *
 * Данный проект создан с целью тестирования навыков разработки на php
 *
 * @author Сhernets Anton Sergeevich
 * @version 1.0
 * @copyright Copyright (c) 2017, Coder UA
 *
 * Не зависит от платформы запуска
 */
new app(substr($_SERVER['REQUEST_URI'],2));
