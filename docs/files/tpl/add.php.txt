<?php

/**
 *
 * отображение страницы добаввления нового комментария
 *
 */

?>

<br>
<br>
<br>

<!-- form add post -->
<div class="container-fluid">
	<div class="container-weight">
		<div class="form_comment">
			<form class="form-horizontal" name="form" action="/?add" method="post">
				вы можете использовать символы :) или :( или :P или все сразу
				<label>Ваше имя</label>
				<input type="text" class="input-xxlarge" name="author" value="<?=@$this->post['author']?>" required/>
				<label>Текст</label>
				<textarea name="text" class="input-xxlarge" required><?=@$this->post['text']?></textarea>
				<br>
				<button class="btn btn-primary" name="submit" type="submit">Добавить</button>
			</form>
		</div>
	</div>
</div>
<!-- /form -->
