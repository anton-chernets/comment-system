<?php

/**
 * Базовый контроллер web приложения
 */

class ctrl {

    /**
     * Метод - конструктор
     *
     * Здесь создается базовый класс
     *
     */

    public function __construct() {
		$this->db = new db();//создание объекта - экземпляра текущей базы данных
	}

    /**
     * Метод подключения визуальных шаблонов для отображения из каталога tpl
     */

    public function out($tplname,$nested=false) {
		if (!$nested) {
			$this->tpl = $tplname;
			include "tpl/main.php";
		} else
			include "tpl/" . $tplname;
	}

}

/**
 * Класс запуска маршрутов web приложения
 *
 */

class app {

    /**
     * Метод выдачи страницы index.php по слэшу или по пути запросу
     */

	public function __construct($path) {

		$this->route = explode('/', $path);//строка на подстроки
		$this->run();
	}

    /**
     * Метод - роутер
     */

	private function run() {

        /**
         * Получаем из массива первый url
         * проверяем его на маленькие лат. буквы и др.
         * если соответствует полностью приложение запускает
         */

		$url = array_shift($this->route);
		if (!preg_match('#^[a-zA-Z0-9.,-]*$#', $url))
			throw new Exception('Invalid path');//исключение ошибки
        /**
         * Отдельная переменная для названия контроллеров
         *
         * @var $ctrlName string контроллер
         */
		$ctrlName = 'ctrl' . ucfirst($url);
		if (file_exists('app/' . $ctrlName.'.php')) {
			$this->runController($ctrlName);
		} else {
			array_unshift($this->route, $url);
			$this->runController('ctrlIndex');
		}
	}

    /**
     * Метод создания нового класса для запуска контроллера
     *
     * При неудовлетворительном запросе к странице будет показана ошибка 404
     */
	private function runController($ctrlName) {
		include "app/" . $ctrlName . ".php";
		$ctrl = new $ctrlName();
		if (empty($this->route) || empty($this->route[0])) {
			$ctrl->index();
		} else {
			if (empty($this->route))
				$method = 'index';
			else
				$method = array_shift($this->route);
			if (method_exists($ctrl, $method)) {
				if (empty($this->route))
				$ctrl->$method();
				else
					call_user_func_array (array($ctrl,$method), $this->route);
			} else
				throw new Exception('Error 404');
		}
	}

}