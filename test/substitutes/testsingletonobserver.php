<?php
    require_once '/../../app/singleton_trait.php';

    class TestSingleton {
        use Singleton;

        public function action($data) {
            return strtoupper($data);
        }
    }
