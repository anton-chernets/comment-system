## Php Task
   Implement a comment system – a form, that allows you to submit comments, save them in the DB, and display the comments. 
   Implement an event manager class. It should implement the singleton pattern, and support pluggable observers. The class will allow you to subscribe to events, fire events, and when the events are fired – call the appropriate callbacks. 
   Create a DB table that will hold a list of observers, and will initialize them one by one.
   Implement an 'onSubmit' event for the comment submission. 
   Write an observer, that subscribes to the 'onSubmit' event for the comment, fires on the submission of a new comment, and replaces all smileys with corresponding images of the smileys. 
   All code should be covered with PHPDoc, and unit tests, including the event manager, the observer, and any other code. 
   
   Keep in mind, that usage of pre-existing frameworks is forbidden (But allow to use composer & phpunit). All DB code should be in a .sql file attached. 
   
   Big plus:
   1. additional observers
   2. events and design patterns
   3. PSR recommendations
   4. last version php and new features
   
   Also, please attach a README file, with explanations as to how to run the code.
   
## Запуск проекта
   This repository contains:
   
   */app/  - необходимый код для работы приложения
   */app/substitutes  - функционал замены 
   */css/  - стили
   */img/  - изображения
   */docs/  - документация phpDocumentor
   */js/  - скрипты
   */lib/ - собственная библиотека по подключению к бд и базовая обработка GET запросов
   */tpl/ - шаблоны для отображения пользователю
   */sql/  - запросы для загрузки таблиц и данных в необходимую базу
   */test/  - phpunit тесты
   *.gitignore - список файлов и каталогов игнорируемых системой контроля версий
   *index.php  - точка входа приложения
   *README   - данный файл

Для запуска на локальном сервере с собственной базой:
1. Создать базу данных и пользователя с паролем в MySQL
2. Сделать импорт таблиц из дампа(файла в корне репозитория) noblog.sql в созданную базу
3. В "lib\db.php" в конструкторе класса прописать параметры подключения к БД

При удачном подключении к базе данных на сервере с проектом станут доступными:
- Публикация комментария (при смайл символах они будут заменены на соответствующие анимированные изображения)
- Просмотр комментариев
- Удаление комментария

# Иллюстрации
![добавление комментария](img/add.png)
![главная страница](img/Comments.png)

# Установка PHPUnit.

С официального сайта https://phar.phpunit.de/ была была скачана версия phpunit-4.8.9.phar

На Windows 
1. Перейти в каталог с PHP.exe, например у меня на OpenServere:
C:\OpenServer\modules\php\PHP-5.5

2.Создать файл phpunit.bat в данном каталоге, чтобы получить доступ к phpunit из любой папки (например из корня своего приложения),
а не только находясь в папке с php куда он установлен: пишем в него @php "%~dp0phpunit-4.8.9.phar" %*

3.Проверим появился ли доступ:
phpunit --version
