<?php

/**
 * Класс контролер
 *
 * Контролер приложения
 *
 */

class ctrlIndex extends ctrl {

    /**
     * Событие базовой страницы приложения index
     *
     */

	function index() {
		$this->comments = $this->db->query("SELECT * FROM comments ORDER BY create_at DESC")->all();
		$this->out('/comments.php');
	}

    /**
     * Событие публикации нового комментария с заменой смайл символов на изображения
     *
     */

	function add() {
		if (!empty($_POST)) {
            require(__DIR__ . '/observers_manager.php');
            ObserverManager::getInstance($this->db->mysqli);
            $comment = EventManager::getInstance()->eventOnSubmit($_POST['text']);
			$this->db->query("INSERT INTO comments(author, text, create_at) VALUES(?, ?,?)", $_POST['author'], $comment,time());
            header("Location: /");
		}
		$this->out('add.php');
	}

    /**
     * Событие удаления комментария
     *
     * @param integer $id идентификатор комментария
     */

	function del($id) {
		$this->db->query("DELETE FROM comments WHERE id = ?",$id);
		header("Location: /");
	}
}