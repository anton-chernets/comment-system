<?php
trait Singleton
{

    /**
     * Объект - экземпляр класса (текущий)
     *
     */

    protected static $instance;

    /**
     * Завершенный метод получения конкретного объекта - экземпляра класса
     *
     */

    final public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
            $init_args = func_get_args();
            call_user_func_array(array(static::$instance, '_init'), $init_args);
        }

        return static::$instance;
    }

    /**
     * Неописанные в данном классе методы
     *
     */

    final private function __construct() {}
    final private function __wakeup() {}
    final private function __clone() {}

    protected function _init() {}
};