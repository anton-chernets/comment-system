<?php

/**
 * Класс менеджера событий
 *
 * Реализовывает одноэлементный шаблон и поддерживает подключаемые наблюдатели.
 * Класс позволит подписаться на события и когда они будут запущены - вызовает соответствующие обратные вызовы.
 *
 */

require_once('singleton_trait.php');

class EventManager {
    use Singleton;//активируем трейт

    protected static $events;

    /**
     * Метод - позволит инициализировать класс - менеджер событий
     *
     */

    protected function _init() {
        static::$events = array();
    }

    public function isEventRegistered($eventName) {
        return isset(static::$events[$eventName]);
    }

    protected function & getEventObservers($eventName) {
        if ($this->isEventRegistered($eventName)) {
            return static::$events[$eventName];
        }
        $rv = NULL; // need this 'cause we return by ref
        return $rv;
  
    }

    public function unregisterEvent($eventName) {
        unset(static::$events[$eventName]);
    }

    public function registerEvent($eventName) {
        if (!$this->isEventRegistered($eventName)) {
            static::$events[$eventName] = array();
        }
    }

    /**
     * Метод - позволит запустить необходимое событие с необходимыми данными для обработки
     *
     * @param $eventName string имя события
     * @param $eventData string
     *
     * @return $eventData string
     */

    public function triggerEvent($eventName, $eventData) {
        if ($this->isEventRegistered($eventName)) {
             $observers = & $this->getEventObservers($eventName);
             foreach ($observers as $callback) {
                 $eventData = call_user_func($callback, $eventData);
             }
             return $eventData;
        } else {
             trigger_error('attempt to trigger nonregistered event', E_USER_ERROR);
        }
    }

    /**
     * Метод - позволит подписаться на сбытия
     *
     * @param $eventName string имя события
     * @param $callback string
     *
     * @return string or array
     */

    public function subscribeObserver($eventName, $callback) {
        if (!$this->isEventRegistered($eventName)) {
             // warning only, subscribing will automatically register one
             trigger_error('attempt to subscribe to nonregistered event', E_USER_WARNING);
        }
        // return the index so the observer can be unsubscribed
        $observers = & $this->getEventObservers($eventName);
        return array_push($observers, $callback);
    }

    /**
     * Метод - позволит отписаться от сбытия
     *
     * @param $eventName string имя события
     * @param $index integer номер
     *
     * @return string
     */

    public function unsubscribeObserver($eventName, $index) {
        if ($this->isEventRegistered($eventName)) {
            $observers = & $this->getEventObservers($eventName);
            if (isset($observers[$index])) {
                $callback = $observers[$index];
                unset($observers[$index]);
            } else {
                $callback = NULL; //уже отписаны
            }

            return $callback;
        } else {
             trigger_error('attempt to unsubscribe from nonregistered event', E_USER_ERROR);
        }
    }

    /**
     * Метод - обратные вызов
     *
     * @param $methodName string имя метода который заменит смайл символы на изображение
     * @param $args string текст комментария
     *
     * @return string
     */

    public function __call($methodName, $args) {
        if (0 === strpos($methodName, "on")) {
            $eventName = strtolower(substr($methodName, 2));
            return $this->subscribeObserver($eventName, $args[0]);
        } elseif (0 === strpos($methodName, "event")) {
            $eventName = strtolower(substr($methodName, 5));
            return $this->triggerEvent($eventName, $args[0]);
        } else {
            trigger_error("unknown method " . $methodName, E_USER_ERROR);
        }
    }
};