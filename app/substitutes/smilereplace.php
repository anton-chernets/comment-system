<?php

    /**
     * Метод замены smile символов на html разметку - изображение
     *
     * @param $text string
     *
     * @return $text string текст комментария
     * @return $replacements array измененная строка
     */

    function smileReplace($text) {
        $replacements = array(
            ':)' => '<img src="/img/jemocii_10.gif" alt=":)" border="0">',
            ':(' => '<img src="/img/jemocii_1.gif" alt=":(" border="0">',
            ':P' => '<img src="/img/jemocii_14.gif" alt=":P" border="0">'
        );

        return strtr($text, $replacements);
    }