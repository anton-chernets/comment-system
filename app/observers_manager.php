<?php

/**
 * Класс менеджер наблюдателей
 *
 * Работает с таблицей DB, которая содержат список наблюдателей,
 * и инициализирует их по имени класса или метода один за другим
 *
 */

require_once 'event_manager.php';
require_once 'singleton_trait.php';

class ObserverManager {
    use Singleton;

    /**
     * Статический массив параметров (наблюдателей)
     *
     */

    protected static $observers = array();

    /**
     * Метод - позволит инициализировать класс - менеджер наблюдателей
     *
     * @param $conn object текущий объект
     *
     */

    protected function _init($conn) {
        $this->initObserversFromDb($conn);
    }

    /**
     * Метод - позволит инициализировать класс - менеджер наблюдателей
     *
     * @param $conn object текущий объект
     *
     */

    protected function initObserversFromDb($conn) {
        $evManager = EventManager::getInstance();

        $query = $conn->prepare(
            "SELECT class_name, method_name, event_name FROM event_manager"
        );
        $query->execute();
        $result = $query->get_result();

        while ($row = $result->fetch_row()) {
            list($singletonClassname, $methodName, $eventName) = $row;
            if ($singletonClassname) { // singleton callback
                $callback = function($eventData) use ($singletonClassname, $methodName) {
                    require('app/' . strtolower($singletonClassname) . '.php');
                    return call_user_func(array($singletonClassname, $methodName), $eventData);
                };
            } else { // simple method callback
                $callback = function($eventData) use ($methodName) {
                    require('app/substitutes/' . strtolower($methodName) . '.php');
                    return call_user_func($methodName, $eventData);
                };
            }
            $evManager->registerEvent($eventName);
            $evManager->subscribeObserver($eventName, $callback);
        }
    }
};