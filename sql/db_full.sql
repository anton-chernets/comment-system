-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Дек 04 2017 г., 16:39
-- Версия сервера: 5.6.34
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `noblog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'автор комментария',
  `text` text CHARACTER SET utf8 NOT NULL COMMENT 'текст комментария',
  `create_at` int(11) NOT NULL COMMENT 'время создания'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `author`, `text`, `create_at`) VALUES
(21, 'Антон Чернец', 'Тестовый комментарий <img src=\"/img/jemocii_10.gif\" alt=\":)\" border=\"0\">  <img src=\"/img/jemocii_14.gif\" alt=\":P\" border=\"0\">  <img src=\"/img/jemocii_1.gif\" alt=\":(\" border=\"0\">', 1512397523);

-- --------------------------------------------------------

--
-- Структура таблицы `event_manager`
--

CREATE TABLE `event_manager` (
  `id` int(11) NOT NULL,
  `class_name` varchar(256) DEFAULT NULL COMMENT 'имя класса',
  `method_name` varchar(256) NOT NULL COMMENT 'имя метода',
  `event_name` varchar(256) NOT NULL COMMENT 'имя события'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `event_manager`
--

INSERT INTO `event_manager` (`id`, `class_name`, `method_name`, `event_name`) VALUES
(1, NULL, 'smileReplace', 'onsubmit');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `event_manager`
--
ALTER TABLE `event_manager`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `event_manager`
--
ALTER TABLE `event_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
